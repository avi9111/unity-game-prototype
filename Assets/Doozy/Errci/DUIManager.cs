﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
using Doozy.Engine.UI;

public class DUIManager : MonoBehaviour {

	string currSceneName;
	UIPopup mPopup;
	// Use this for initialization
	public UIView[] views;
	void Start () {
		////没什么作用的代码。。。。。。。。
		////UIPopupManager.A
		//UICanvas canvas = UICanvas.CreateUICanvas("MyNewCanvas");

		//currSceneName = "Base";
		//TODO:UIView 暂时隐藏所有,一开始加载还是慢的， 最终还是要优化
		foreach (var v in views) {
			v.gameObject.SetActive(false);
		}
		
	}

	public void DoSwitchScene() {
		Debug.LogError("dod do do");

	}
	public void DoChangeCanvas() { 
		
	}
	public void DoPopUp() {
		Debug.Log("Pop up");

		
		if (mPopup == null)
		{
			mPopup = UIPopupManager.GetPopup("popLuan");
			//阅读代码后发现，无论UIPopupManager，还是UIPopup->GetPopup()后会克隆一份，所以必须通过模组级别变量持有，
			//否则，一直New Popup对象，都是全新的，并不是我们真正需要的那一个Popup实例
			//也就是下面代码都没什么卵用
			//popup = UIPopup.GetPopup("popLuan");
			//UICanvas canvas = popup.GetTargetCanvas();
			//popup.Container.RectTransform = canvas.RectTransform;
			//popup.Overlay.RectTransform = popup.Container.RectTransform;
			//UIPopupManager.AddToQueue(popup);
		}

		UICanvas canvas = mPopup.GetTargetCanvas();
		mPopup.Container.RectTransform = canvas.RectTransform;
		mPopup.Overlay.RectTransform = mPopup.Container.RectTransform;
		///////////////// 还特喵的需要补上这一句（才能Show和Hide安全调用）//////////////////
		if (canvas.gameObject.activeSelf == false)
			canvas.gameObject.SetActive(true);
		////////////////////////////////////////////////////////////////////////////////////
		mPopup.Show();
		
	}
}
