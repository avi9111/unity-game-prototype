﻿using Doozy.Engine.UI.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Doozy.Engine.UI
{
    public class UICheckUtil
    {
        public static bool CheckVec(Vector3 vec)
        {
            if (vec.x != 0) return true;
            if (vec.y != 0) return true;
            if (vec.z != 0) return true;
            return false;
        }
        public static bool IsSetMove(Move move) {
            return CheckVec(move.From) || CheckVec(move.To);
        }
        public static bool IsSetRotate(Rotate rotate) {
            return CheckVec(rotate.From) || CheckVec(rotate.To);
        }

        public static bool IsSetScale(Scale scale) {
            return CheckVec(scale.From) || CheckVec(scale.To);
        }

        public static bool IsSetFade(Fade f) {
            return f.From!=0 || f.To!=0;
        }
    }
}
